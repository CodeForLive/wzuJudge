import datetime
import logging

from django.contrib.auth.decorators import login_required
from django.core.paginator import InvalidPage, PageNotAnInteger, EmptyPage, Paginator
from django.db.models import F
from django.http import JsonResponse
from django.shortcuts import render
from django.db import transaction
# Create your views here.
from django.utils import timezone
from django.utils.timezone import utc

from user.check_permission import superuser_only
from user.models import Contest, UserInfo, Problem, Problem_To_Contest, Submit, Contest_To_UserInfo, Code, \
    Contest_User_Problem, Announcement


def get_contestants(contest_id):
    """获取contants"""
    contest = Contest.objects.filter(id=contest_id).first()
    ctus = Contest_To_UserInfo.objects.filter(contest=contest)
    contestants = []
    for ctu in ctus:
        contestants.append(ctu.user.id)
    return contest,contestants

def contest_list(request):
    contests = Contest.objects.all().order_by('status')
    for contest in contests:
        if contest.end_time <= timezone.now():
            Contest.objects.filter(id=contest.id).update(status=2)
            contests = Contest.objects.all().order_by('status')  # 重新获取比赛数据
        elif contest.start_time <= timezone.now() and contest.end_time > timezone.now():
            Contest.objects.filter(id=contest.id).update(status=1)  # 重新获取比赛数据
            contests = Contest.objects.all().order_by('status')
        elif contest.start_time > timezone.now():
            Contest.objects.filter(id=contest.id).update(status=0)  # 重新获取比赛数据
            contests = Contest.objects.all().order_by('status')
    search = request.GET.get('search')
    if search is None:
        contests = Contest.objects.all().order_by('status')
    else:
        contests = Contest.objects.filter(title__icontains=search).order_by('status')
    item_num = 5  # 显示条数
    current_page = request.GET.get('page', 1)  # 获取当前页数，默认值为1
    from extras.paging_device import paging
    contests, page_list, num_pages = paging(contests, item_num, current_page)
    # context = {
    #     'problems': problems.object_list,
    #     'search': search,
    #     'num_pages': num_pages,
    #     'page_list': page_list,
    #     'page_obj': contests,
    #     'current_page': current_page,
    # }
    contest_list = []
    for contest in contests.object_list:
        seconds = (contest.end_time - contest.start_time).total_seconds()
        m,s = divmod(seconds,60)
        h,m = divmod(m,60)
        time = "%02d:%02d:%02d"%(h,m,s)
        contest_list.append([contest,time])
    context  = {
        'contests': contest_list,
        'search': search,
        'num_pages': num_pages,
        'page_list': page_list,
        'page_obj': contests,
        'current_page': current_page,
    }
    return render(request,'contest_list.html', context=context)


def contest_detail(request,contest_id):
    if request.is_ajax():
        res = {'status':False,'msg':None}
        if not request.user.is_authenticated:
            res['msg'] = '请先登录'
            return JsonResponse(res)
        try:
            password = request.POST.get('enter_password')
            contest = Contest.objects.filter(id=contest_id, password=password).first()
        except Exception as e:
            print(e)
            res['msg'] = '密码错误'
            return JsonResponse(res)
        try:
            with transaction.atomic():
                ctu = Contest_To_UserInfo.objects.create(contest=contest,user=request.user)
                ptcs = Problem_To_Contest.objects.filter(contest__id=contest_id)
                for cptu in ptcs:
                    Contest_User_Problem.objects.create(contest_To_User=ctu, problem_id=cptu.problem.id)
        except Exception as e:
            print(e)
            res['msg'] = '加入比赛失败'
            return JsonResponse(res)
        res['status'] = True
        res['msg'] = '欢迎进入比赛'
        return JsonResponse(res)
    contest = Contest.objects.filter(id=contest_id).first()
    # 刷新比赛状态
    if contest.end_time <= timezone.now():
        Contest.objects.filter(id=contest.id).update(status=2)
        contest = Contest.objects.filter(id=contest_id).first() # 重新获取比赛数据
    elif contest.start_time <= timezone.now() and contest.end_time > timezone.now():
        Contest.objects.filter(id=contest.id).update(status=1) # 重新获取比赛数据
        contest = Contest.objects.filter(id=contest_id).first()
    elif contest.start_time > timezone.now():
        Contest.objects.filter(id=contest.id).update(status=0)  # 重新获取比赛数据
        contest = Contest.objects.filter(id=contest_id).first()
    user = UserInfo.objects.filter(id=contest.creator_id).first()
    ctus = Contest_To_UserInfo.objects.filter(contest=contest)
    contestants = []
    for ctu in ctus:
        contestants.append(ctu.user.id)
    context = {
        'flag': 1,  # 用于标记哪个右侧栏的导航标签
        'contest_id': contest_id,
        'contest':contest,
        'user':user,
        'contestants': contestants,
    }
    return render(request,'contest_detail.html',context=context)


@superuser_only
def edit_contest(request,contest_id):
    contest = Contest.objects.get(id=contest_id)
    ptcs = Problem_To_Contest.objects.filter(contest_id=contest_id)
    if request.is_ajax():
        res = {'status': False, 'msg': None}
        c_contest_name = request.POST.get('c_contest_name')
        c_protected = request.POST.get('c_protected', 0)
        c_password = request.POST.get('c_password', '')
        c_start_time = request.POST.get('c_start_time').replace("T", " ")
        c_end_time = request.POST.get('c_end_time').replace("T", " ")
        start_time = timezone.datetime.strptime(c_start_time, '%Y-%m-%d %H:%M')
        start_time = start_time - datetime.timedelta(hours=8)
        start_time = start_time.replace(tzinfo=utc)
        end_time = timezone.datetime.strptime(c_end_time, '%Y-%m-%d %H:%M')
        end_time = end_time - datetime.timedelta(hours=8)
        end_time = end_time.replace(tzinfo=utc)
        c_description = request.POST.get('c_description', '')
        c_orders = request.POST.getlist('c_orders[]')
        c_ids = request.POST.getlist('c_ids[]')

        try:
            with transaction.atomic():
                Contest.objects.filter(id=contest_id).update(title=c_contest_name, type=int(c_protected), password=c_password,
                                                 start_time=start_time,
                                                 end_time=end_time, description=c_description,
                                                 creator_id=request.user.id)
                ptcs = contest.problem_to_contest_set.all()
                for ptc in ptcs:
                    ptc.delete()
                for i in range(len(c_ids)):
                    problem = Problem.objects.get(id=int(c_ids[i].strip()))
                    Problem_To_Contest.objects.create(problem=problem, contest=contest, p_id=c_orders[i])
        except Exception as e:
            print(repr(e))
            res['msg'] = "修改比赛失败"
            return JsonResponse(res)
        res['status'] = True
        res['msg'] = "修改比赛成功"
        return JsonResponse(res)
    context = {
        'ptcs': ptcs,
        'contest': contest,
    }
    return render(request,'contest_edit.html',context=context)


@superuser_only
def create_announcement(request,contest_id):
    if request.is_ajax():
        res = {'status':False ,'msg':None,'url':None}
        title = request.POST.get('title')
        description = request.POST.get('description')
        try:
            contest = Contest.objects.get(id=contest_id)
            Announcement.objects.create(title=title,description=description,contest=contest,create_author=request.user)
        except Exception as e:
            res['msg'] = '创建公告失败'
            return JsonResponse(res)
        res['status'] = True
        res['msg'] = '创建成功'
        res['url'] = '/index'
        return JsonResponse(res)
    context = {
        'contest_id':contest_id
    }
    return render(request, 'create_announcement.html',context=context)

@login_required(login_url='/user/sign_in')
def announcements(request,contest_id):
    try:
        contest, contestants = get_contestants(contest_id)
    except Exception as e:
        return
    announcements = contest.announcement_set.all().order_by("-create_time")

    context = {
        'announcements': announcements,
        'flag': 2, # 用于标记哪个右侧栏的导航标签
        'contest_id': contest_id,
        'contest': contest,
        'contestants': contestants,
    }
    return render(request,'contest_announcement.html',context=context)


@login_required(login_url='/user/sign_in')
def problem_list(request,contest_id):
    # contest = Contest.objects.filter(id=contest_id).first()
    contest, contestants = get_contestants(contest_id)
    ptcs = contest.problem_to_contest_set.all()

    cups = Contest_User_Problem.objects.filter(contest_To_User__contest_id=contest_id,contest_To_User__user=request.user,ac_number__gte=1)
    # cup = Contest_User_Problem.objects.get(id=1)
    problems = []
    for cup in cups:
        problems.append(cup.problem_id)

    context = {
        'flag':3, # 用于标记哪个右侧栏的导航标签
        'ptcs': ptcs,
        'contest_id': contest_id,
        'problems_id': problems,
        'contest': contest,
        'contestants': contestants,
    }
    return render(request,'contest_problem_list.html',context=context)

@login_required(login_url='/user/sign_in')
def problem_detail(request,contest_id,p_id):
    contest, contestants = get_contestants(contest_id)
    ptc = Problem_To_Contest.objects.filter(contest__id=contest_id,p_id=p_id).first()
    # ptc = Problem_To_Contest.objects.get(contest__id=contest_id,p_id=p_id)
    problem_id = ptc.problem.id
    if request.is_ajax():
        res = {'status':False,'msg':None}
        try:
            contest = Contest.objects.get(id=contest_id)
            if contest.end_time < timezone.now():
                res['msg'] = '比赛已结束，提交失败'
                return JsonResponse(res)
        except Exception as e:
            res['msg'] = '提交失败'
            return JsonResponse(res)
        code = request.POST.get('code')
        language = request.POST.get('language')

        try:
            with transaction.atomic():  # 事务
                submit = Submit.objects.create(language=language, contest_id=contest_id, problem_id=problem_id,
                                               user_id=request.user.id)
                Code.objects.create(content=code,submit=submit)
                Problem_To_Contest.objects.filter(contest_id=contest_id,problem_id=problem_id).update(total_num=F('total_num')+1)
                ctu = Contest_To_UserInfo.objects.filter(contest_id=contest_id,user=request.user).first()
                # 判断是否已将用户加入比赛，若未加入比赛则加入比赛，并创建cup
                if ctu is None:
                    ctu = Contest_To_UserInfo.objects.create(contest_id=contest_id,total_number=1, user=request.user)
                    ptcs = Problem_To_Contest.objects.filter(contest__id=contest_id)
                    for cptu in ptcs:
                        Contest_User_Problem.objects.create(contest_To_User=ctu, problem_id=cptu.problem.id)
                else:
                    Contest_To_UserInfo.objects.filter(contest_id=contest_id, user_id=request.user.id).update(
                        total_number=F('total_number') + 1)
                res['status'] = True
        except Exception as e:
            res['msg'] = '提交失败，请重新提交'
        return JsonResponse(res)
    context = {
        'flag':3, # 用于标记哪个右侧栏的导航标签
        'ptc': ptc,
        'contest_id': contest_id,
        'contest': contest,
        'contestants': contestants,
    }

    return render(request,'contest_problem_detail.html',context=context)


@login_required(login_url='/user/sign_in')
def rank(request,contest_id):
    # contest = Contest.objects.filter(id=contest_id).first()
    contest, contestants = get_contestants(contest_id)

    ctus = Contest_To_UserInfo.objects.filter(contest_id=contest_id).order_by('-ac_number')
    # ctu = Contest_To_UserInfo.objects.get(contest_id=contest_id)
    # ctu.user.username
    # ctu.contest_user_problem_set
    # contest.problem_to_contest_set
    sorted_ctus = []
    for ctu in ctus:
        if ctu.total_time != 0:
            x = ctu.total_time.split(':')
            if len(x) == 1:
                x.append('0')
                x.append('0')
            elif len(x) == 2:
                x.append('0')
            a = '%02d%02d%02d' %(int(x[0]),int(x[1]),int(x[2]))
        else:
            a=0
        a = int(a)
        sorted_ctus.append([a,ctu])
    tmp = sorted(sorted_ctus, key=lambda x: (-x[1].ac_number,x[0]))
    # print(tmp)
    ctus = []
    # ctus = tmp[:][:][1]
    for t in tmp:
        ctus.append(t[1])
        # print(t[1])
    # print(ctus)
    n = contest.problem_to_contest_set.all().count()
    width = 200
    if n > 4:
        width = 1500 / (n + 4)

    item_num = 30  # 显示条数
    current_page = request.GET.get('page', 1)  # 获取当前页数，默认值为1
    from extras.paging_device import paging
    ctus, page_list, num_pages = paging(ctus, item_num, current_page)
    context = {
        'flag':5, # 用于标记哪个右侧栏的导航标签
        'contest_id':contest_id,
        'contest': contest,
        'contestants': contestants,
        'width': width,
        'ctus': ctus.object_list,
        'num_pages': num_pages,
        'page_list': page_list,
        'page_obj': ctus,
        'current_page': current_page,
    }
    return render(request,'contest_rank.html',context=context)

@login_required(login_url='/user/sign_in')
def status_list(request,contest_id):
    contest, contestants = get_contestants(contest_id)
    item_num = 30  # 显示条数
    title = Contest.objects.get(id=contest_id).title

    mine = request.GET.get('mine', 'false')
    if request.user.id == None:
        mine = 'false'
    get_status = request.GET.get('status')
    search = request.GET.get('search')
    current_page = request.GET.get('page', 1)  # 获取当前页数，默认值为1
    all_status = {
        "All": -1,
        "Waiting": 0,
        "Accepted": 1,
        "Time Limit Exceeded": 2,
        "Memory Limit Exceeded": 3,
        "Wrong Answer": 4,
        "Runtime Error": 5,
        "Output limit": 6,
        "Compile Error": 7,
        "Presentation Error": 8,
        "System Error": 11,
        "Judging": 12}

    try:  # 防止获取的status字符串不在字典内
        get_status = all_status[get_status]
    except KeyError:
        logging.warning(TypeError)
    except Exception:
        logging.warning(Exception)

    try:  # 防止获取的status字符串不为整数
        get_status = int(get_status)
    except TypeError:
        get_status = -1
        logging.warning(TypeError.args)
    except Exception:
        logging.warning(Exception)

    if mine == 'true':
        user_id = request.user.id
        if get_status > -1 and search is None:  # 只获取到状态码
            submissions = Submit.objects.filter(contest_id=contest_id,code__result=get_status, user__id=user_id).order_by(
                '-solution_id')  # 倒叙遍历提交表
        elif get_status > -1 and search is not None:  # 同时获取到状态与搜索字符串
            submissions = Submit.objects.filter(contest_id=contest_id,code__result=get_status, problem__title__icontains=search,
                                                user__id=user_id).order_by('-solution_id')
        elif get_status == -1 and search is not None:  # 只获取到搜索字符串
            submissions = Submit.objects.filter(contest_id=contest_id,problem__title__icontains=search, user__id=user_id).order_by(
                '-solution_id')
        else:  # 初始页面，状态码与搜索字符串均为获得
            submissions = Submit.objects.filter(contest_id=contest_id,user__id=user_id).order_by('-solution_id')  # 倒叙遍历提交表
    else:
        if get_status > -1 and search is None:  # 只获取到状态码
            submissions = Submit.objects.filter(contest_id=contest_id,code__result=get_status).order_by('-solution_id')  # 倒叙遍历提交表
        elif get_status > -1 and search is not None:  # 同时获取到状态与搜索字符串
            submissions = Submit.objects.filter(contest_id=contest_id,code__result=get_status, problem__title__icontains=search).order_by(
                '-solution_id')
        elif get_status == -1 and search is not None:  # 只获取到搜索字符串
            submissions = Submit.objects.filter(contest_id=contest_id,problem__title__icontains=search).order_by(
                '-solution_id')
        else:  # 初始页面，状态码与搜索字符串均为获得
            submissions = Submit.objects.filter(contest_id=contest_id,).order_by('-solution_id')  # 倒叙遍历提交表

    # 分页操作
    # paginator = Paginator(submissions, item_num)
    # try:
    #     submit_list = paginator.get_page(current_page)
    # except  (InvalidPage, PageNotAnInteger, EmptyPage) as e:
    #     logging.warning(e)
    #     submit_list = paginator.get_page(1)
    # except Exception:
    #     logging.warning(Exception)
    #     submit_list = paginator.get_page(1)
    #
    # if int(submit_list.number) == 1:
    #     # 总页数超过3页
    #     if (int(submit_list.number) + 2) <= paginator.num_pages:
    #         page_num = range(int(submit_list.number), int(submit_list.number) + 3)
    #     else:
    #         page_num = range(int(submit_list.number), int(paginator.num_pages) + 1)
    #     # 判断是否是最后一页
    # elif not submit_list.has_next():
    #     if (int(submit_list.number) - 2) > 0:
    #         page_num = range(int(submit_list.number) - 2, int(submit_list.number) + 1)
    #     else:
    #         page_num = range(1, int(submit_list.number) + 1)
    # else:
    #     if (int(submit_list.number) - 1) > 0 and (int(submit_list.number) + 1) <= paginator.num_pages:
    #         page_num = range(int(submit_list.number) - 1, int(submit_list.number) + 2)
    #     elif (int(submit_list.number) - 1) <= 0 and (int(submit_list.number) + 1) <= paginator.num_pages:
    #         page_num = range(1, int(submit_list.number) + 2)
    #     else:
    #         page_num = range(1, int(submit_list.number) + 1)
    #
    # print(page_num)
    from extras.paging_device import paging
    submit_list, page_list, num_pages = paging(submissions, item_num, current_page)
    context = {
        'flag': 4, # 用于标记哪个右侧栏的导航标签
        'contest_id':contest_id,
        'title':title,
        'submissions': submit_list.object_list,
        'page_obj': submit_list,
        'page_list': page_list,
        'current_page': current_page,
        'get_status': get_status,
        'search': search,
        'mine': mine,
        'contest': contest,
        'contestants': contestants,
        'num_pages':num_pages,
    }
    return render(request,'contest_status_list.html',context=context)