import logging

from django.shortcuts import render, redirect

# Create your views here.
from user.models import Submit,Code
from django.core.paginator import Paginator, InvalidPage, PageNotAnInteger, EmptyPage
import math


def status(request):
    # print(request.user.id)

    item_num = 5 # 显示条数

    mine = request.GET.get('mine','false')
    if request.user.id == None:
        mine='false'
    get_status = request.GET.get('status')
    search = request.GET.get('search')
    current_page = request.GET.get('page',1) # 获取当前页数，默认值为1
    all_status = {
    "All":-1,
    "Waiting": 0,
    "Accepted": 1,
    "Time Limit Exceeded": 2,
    "Memory Limit Exceeded": 3,
    "Wrong Answer": 4,
    "Runtime Error": 5,
    "Output limit": 6,
    "Compile Error": 7,
    "Presentation Error": 8,
    "System Error": 11,
    "Judging": 12}


    try: # 防止获取的status字符串不在字典内
        get_status = all_status[get_status]
    except KeyError:
        logging.warning(TypeError)
    except Exception:
        logging.warning(Exception)


    try: # 防止获取的status字符串不为整数
        get_status = int(get_status)
    except TypeError:
        get_status = -1
        logging.warning(TypeError.args)
    except Exception:
        logging.warning(Exception)

    if mine == 'true':
        user_id = request.user.id
        if get_status > -1 and search is None:  # 只获取到状态码
            submissions = Submit.objects.filter(code__result=get_status,user__id=user_id,contest=None).order_by('-solution_id')  # 倒叙遍历提交表
        elif get_status > -1 and search is not None:  # 同时获取到状态与搜索字符串
            submissions = Submit.objects.filter(code__result=get_status,problem__title__icontains=search,user__id=user_id,contest=None).order_by('-solution_id')
        elif get_status == -1 and search is not None: # 只获取到搜索字符串
            submissions = Submit.objects.filter(problem__title__icontains=search,user__id=user_id,contest=None).order_by(
                '-solution_id')
        else:  # 初始页面，状态码与搜索字符串均为获得
            submissions = Submit.objects.filter(user__id=user_id,contest=None).order_by('-solution_id')  # 倒叙遍历提交表
    else:
        if get_status > -1 and search is None:  # 只获取到状态码
            submissions = Submit.objects.filter(code__result=get_status,contest=None).order_by('-solution_id')  # 倒叙遍历提交表
        elif get_status > -1 and search is not None:  # 同时获取到状态与搜索字符串
            submissions = Submit.objects.filter(code__result=get_status,problem__title__icontains=search,contest=None).order_by('-solution_id')
        elif get_status == -1 and search is not None: # 只获取到搜索字符串
            submissions = Submit.objects.filter(problem__title__icontains=search,contest=None).order_by(
                '-solution_id')
        else:  # 初始页面，状态码与搜索字符串均为获得
            submissions = Submit.objects.filter(contest=None).order_by('-solution_id')  # 倒叙遍历提交表

    # 分页操作
    item_num = 30  # 显示条数
    current_page = request.GET.get('page', 1)  # 获取当前页数，默认值为1
    from extras.paging_device import  paging
    submit_list,page_list,num_pages = paging(submissions,item_num,current_page)
    context = {
        'submissions': submit_list.object_list,
        'page_obj': submit_list,
        'page_list': page_list,
        'current_page': current_page,
        'get_status': get_status,
        'search': search,
        'mine':mine,
        'num_pages':num_pages,
    }
    return render(request, 'status_list.html', context=context)


def detail(request, solution_id):
    # 用户未登录
    if request.user.is_anonymous:
        return redirect('/404')
    # 户为超级用户
    elif request.user.is_superuser:
        submit = Submit.objects.filter(solution_id=solution_id).first()

    else:
        # 判断非超级用户的情况下时候为当前用户的提交
        submit = Submit.objects.filter(user__id=request.user.id,solution_id=solution_id).first()

        if not submit:
            return redirect('/404')

    context = {
        'submit': submit
    }
    return render(request, 'status_detail.html', context=context)
