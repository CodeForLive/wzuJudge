DROP TRIGGER IF EXISTS add_user_ac_number;
CREATE TRIGGER add_user_ac_number AFTER UPDATE ON user_code FOR EACH ROW
BEGIN
	IF new.result=1 THEN
		SELECT user_id INTO @user_id FROM user_submit WHERE user_submit.solution_id = new.submit_id;
		select count(*) into @ac_n from user_code where submit_id in (select problem_id from user_submit where user_id=@user_id) and result=1 GROUP BY result;
		UPDATE user_userinfo set user_userinfo.ac_number=@ac_n WHERE id=@user_id;
	END IF;
END
;;
delimiter ;

DROP TRIGGER IF EXISTS `add_user_ac_number`;
delimiter ;;
CREATE TRIGGER `add_user_ac_number` AFTER UPDATE ON `user_code` FOR EACH ROW BEGIN
	IF new.result=1 THEN
		SELECT user_id INTO @user_id FROM user_submit WHERE user_submit.solution_id = new.submit_id;
		select count(*) into @ac_n from user_code where submit_id in (select problem_id from user_submit where user_id=@user_id) and result=1 GROUP BY result;
		UPDATE user_userinfo set user_userinfo.ac_number=@ac_n WHERE id=@user_id;
	END IF;
END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table user_submit
-- ----------------------------
DROP TRIGGER IF EXISTS `insert_into_tmp`;
delimiter ;;
CREATE TRIGGER `insert_into_tmp` AFTER INSERT ON `user_submit` FOR EACH ROW BEGIN
	INSERT into tmp_submit(solution_id,problem_id,pro_lang,user_id,contest_id) VALUES(new.solution_id,new.problem_id,new.language,new.user_id,new.contest_id);
    UPDATE user_userinfo set user_userinfo.total_number=user_userinfo.total_number+1 WHERE id=new.user_id;
END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table user_submit
-- ----------------------------
DROP TRIGGER IF EXISTS `add_user_submissions`;
delimiter ;;
CREATE TRIGGER `add_user_submissions` AFTER INSERT ON `user_submit` FOR EACH ROW BEGIN
	UPDATE user_userinfo set user_userinfo.total_number=user_userinfo.total_number+1 WHERE id=new.user_id;
END
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
