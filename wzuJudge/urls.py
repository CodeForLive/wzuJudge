"""wzuJudge URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include, re_path
from django.views.static import serve

from home import views
from wzuJudge import settings

urlpatterns = [
    path('admin/', admin.site.urls),
    path('',views.index),
    path('index',views.index),
    path('404',views.page_not_found),
    path('home/',include('home.urls')),
    path('user/', include('user.urls')),
    path('problem',include('problem.urls')),
    path('status',include('status.urls')),
    path('rank',include('rank.urls')),
    path('contest',include('contest.urls')),
    re_path(r'^media/(?P<path>.*)$', serve, {'document_root': settings.MEDIA_ROOT}),

]
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATICFILES_DIRS)

# handler400 = views.bad_request
# handler403 = views.permission_denied
handler404 = views.page_not_found
handler500 = views.error