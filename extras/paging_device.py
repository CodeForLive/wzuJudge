import logging

from django.core.paginator import Paginator, InvalidPage, PageNotAnInteger, EmptyPage


def paging(data,page_size,current_page):
    paginator = Paginator(data, page_size)
    try:
        page_obj = paginator.get_page(current_page)
    except  (InvalidPage,PageNotAnInteger,EmptyPage) as e:
        logging.warning(e)
        page_obj = paginator.get_page(1)
    except Exception:
        logging.warning(Exception)
        page_obj = paginator.get_page(1)

    if int(page_obj.number) == 1:
        # 总页数超过3页
        if (int(page_obj.number) + 2) <= paginator.num_pages:
            page_list = range(int(page_obj.number), int(page_obj.number) + 3)
        else:
            page_list = range(int(page_obj.number), int(paginator.num_pages) + 1)
        # 判断是否是最后一页
    elif not page_obj.has_next():
        if (int(page_obj.number) - 2) > 0:
            page_list = range(int(page_obj.number) - 2, int(page_obj.number) + 1)
        else:
            page_list = range(1, int(page_obj.number) + 1)
    else:
        if (int(page_obj.number) - 1) > 0 and (int(page_obj.number) + 1) <= paginator.num_pages:
            page_list = range(int(page_obj.number) - 1, int(page_obj.number) + 2)
        elif (int(page_obj.number) - 1) <= 0 and (int(page_obj.number) + 1) <= paginator.num_pages:
            page_list = range(1, int(page_obj.number) + 2)
        else:
            page_list = range(1, int(page_obj.number) + 1)
    
    return page_obj,page_list,paginator.num_pages