import os

from django.db import transaction
from django.db.models import F
from django.http import JsonResponse
from django.shortcuts import render

# Create your views here.
from user.check_permission import superuser_only
from user.models import Problem, Submit, Code, UserInfo, Simple_Put


def problem_list(request):
    search = request.GET.get('search')
    if request.user.is_superuser:

        if search is not None:
            problems = Problem.objects.filter(title__icontains=search)
        else:
            problems =Problem.objects.all()
    else:
        if search is not None:
            problems = Problem.objects.filter(hide=0,title__icontains=search)
        else:
            problems =Problem.objects.filter(hide=0)

    item_num = 30  # 显示条数
    current_page = request.GET.get('page', 1)  # 获取当前页数，默认值为1
    from extras.paging_device import paging
    problems, page_list, num_pages = paging(problems, item_num, current_page)
    context={
        'problems': problems.object_list,
        'search': search,
        'num_pages': num_pages,
        'page_list': page_list,
        'page_obj': problems,
        'current_page': current_page,
    }
    return render(request,'problem_list.html',context=context)

def problem_detail(request,problem_id):
    if request.method == "POST":
        res = {'status':False,'msg':None}
        if not request.user.is_authenticated:
            res['msg'] = "请先登录"
            return JsonResponse(res)
        code = request.POST.get('code')
        language = request.POST.get('language')
        print(code,language,problem_id,request.user.id)
        problem = Problem.objects.filter(id=problem_id).update(submit_number=F('submit_number')+1)
        user = UserInfo.objects.filter(id=request.user.id).update(total_number=F('total_number')+1)
        submit = Submit.objects.create(language=language,problem_id=problem_id,user=request.user)
        code = Code.objects.create(content=code,submit=submit)
        if code:
            res['status'] = True
        else:
            res['msg'] = '提交失败'
        return JsonResponse(res)

    problem = Problem.objects.filter(id=problem_id).first()
    context = {
        'problem':problem
    }
    return render(request,'problem_detail.html',context=context)


@superuser_only
def problem_edit(request,problem_id):
    if request.is_ajax():
        res = {'status': False, 'msg': None}
        title = request.POST.get('title')
        p_description = request.POST.get('p_description')
        p_input = request.POST.get('p_input')
        p_output = request.POST.get('p_output')
        p_hint = request.POST.get('p_hint')
        simples = request.POST.getlist('simples')
        hide = request.POST.get('hide')
        type = request.POST.get('type').strip()
        if type == 'Standard':
            type = 0
        elif type == "Special":
            type = 1
        else:
            type = 0
        limit_time = request.POST.get('limit_time')
        limit_mem = request.POST.get("limit_mem", 0)
        level = request.POST.get('level','Mid').strip()
        test_case = request.FILES.get('test_case', 0)
        with transaction.atomic():
            Problem.objects.filter(id=problem_id).update(title=title, content=p_description, input=p_input, output=p_output,
                                             hint=p_hint,
                                             hide=hide, rule=int(type), level=level, time_limit=int(limit_time),
                                             mem_limit=int(limit_mem))
            problem = Problem.objects.get(id=problem_id)
            for simple in problem.simple_put_set.all():
                simple.delete()

            for i in range(0, len(simples), 2):
                s_in = simples[i]
                s_out = simples[i + 1]
                simple_put = Simple_Put.objects.create(simple_input=s_in, simple_output=s_out, problem=problem)
                # problem.simple.add(simple_put)
        if test_case:
            t = os.popen('cd ~ ; pwd')
            home_path = t.read()
            home_path = home_path.rstrip()
            project_path = '/acmjudger-master/testcase/'
            tmp = "tmp/"
            filename = home_path + project_path + str(problem.id) + '.zip'
            with open(filename, 'wb') as f:
                for line in test_case:
                    f.write(line)
            try:
                # new_filename = home_path + project_path + tmp + str(problem.id) + ".zip"
                # os.system('mv ' + filename + " " + new_filename)
                case_path = home_path + project_path + str(problem.id)
                os.system('rm -rf ' + case_path)
                os.system('unzip ' + filename + ' -d ' + home_path + project_path + str(problem.id))
                res['status'] = True
                res['msg'] = '修改成功'
            except Exception:
                res['msg'] = "文件存储错误"
        else:
            res['status'] = True
            res['msg'] = '修改成功'

        return JsonResponse(res)
    problem = Problem.objects.get(id=problem_id)
    simples = problem.simple_put_set.all()
    context = {
        'problem': problem,
        'simples': simples,
    }

    return render(request,'edit_problem.html',context=context)

