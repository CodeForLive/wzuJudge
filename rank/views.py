from django.shortcuts import render

# Create your views here.
from user.models import UserInfo


def rank_list(request):
    search = request.GET.get('search')
    if search is None:
        users= UserInfo.objects.all().order_by('-ac_number')
    else:
        users = UserInfo.objects.filter(username__icontains=search).order_by('-ac_number')
    item_num = 30  # 显示条数
    current_page = request.GET.get('page', 1)  # 获取当前页数，默认值为1
    from extras.paging_device import paging
    users, page_list, num_pages = paging(users, item_num, current_page)
    context  = {
        'users': users.object_list,
        'search' : search,
        'num_pages': num_pages,
        'page_list': page_list,
        'page_obj': users,
        'current_page': current_page,
    }
    return render(request,'rank_list.html',context=context)