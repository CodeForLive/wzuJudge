from django.contrib import admin

# Register your models here.

from .models import *

admin.site.register(UserInfo)
admin.site.register(Problem)
admin.site.register(Contest_To_UserInfo)
admin.site.register(Contest)
admin.site.register(Code)
admin.site.register(Contest_User_Problem)
admin.site.register(Submit)
admin.site.register(Problem_To_Contest)
admin.site.register(Simple_Put)
admin.site.register(UserInfo_To_Problem)