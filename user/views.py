import datetime
import os

from django.contrib import auth
from django.db import transaction
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
# Create your views here.
from django.utils import timezone
from django.utils.timezone import utc
import pytz
from django.views.decorators.csrf import csrf_exempt

from user.check_permission import superuser_only
from user.forms import RegisterForm
from user.models import UserInfo, Problem, Simple_Put, Contest, Problem_To_Contest, Announcement


def sign_in(request):
    if request.is_ajax():
        username = request.POST.get("username")
        password = request.POST.get("password")
        valid_code = request.POST.get("valid_code")
        next = request.GET.get("next",'/index')
        res = {"state": False, "msg": None,"url": next}
        s_valid_code = request.session.get("valid_code")
        # 判断验证码是否正确
        if valid_code.upper() == s_valid_code.upper():
            user = auth.authenticate(username=username, password=password)
            if user:  # 若此用户存在则进行登录操作
                res['state'] = True
                auth.login(request, user)
            else:
                res['msg'] = '用户名或者密码错误'
        else:
            res['msg'] = '验证码错误'

        return JsonResponse(res)

    return render(request, 'signin.html')


def get_valid_img(request):
    from PIL import Image
    from PIL import ImageDraw, ImageFont
    from random import randint
    import random
    width = 160
    height = 40

    def get_random_color():
        return (randint(0, 255), randint(0, 255), randint(0, 255))

    def get_background_color():
        return (randint(200, 255), randint(200, 255), randint(200, 255))

    def get_font_color():
        return (randint(0, 100), randint(0, 100), randint(0, 100))

    image = Image.new('RGB', (width, height), get_background_color())

    draw = ImageDraw.Draw(image)
    font = ImageFont.truetype("static/assets/fonts/kumo.ttf", 32)
    temp = []
    for i in range(5):
        random_num = str(randint(0, 9))
        random_low_alpha = chr(randint(97, 122))
        random_upper_alpha = chr(randint(65, 90))
        random_char = random.choice([random_num, random_low_alpha, random_upper_alpha])
        draw.text((24 + i * 24, randint(0, 15)), random_char, get_font_color(), font=font)

        # 保存随机字符
        temp.append(random_char)

    # 噪点

    for i in range(20):
        draw.point([random.randint(0, width), random.randint(0, height)], fill=get_random_color())
        x = random.randint(0, width)
        y = random.randint(0, height)
        # 弧线
        draw.arc((x, y, x + 4, y + 4), 0, 90, fill=get_random_color())

    # 在内存中生成图片
    from io import BytesIO
    f = BytesIO()
    image.save(f, 'png')
    data = f.getvalue()
    f.close()

    valid_code = "".join(temp)
    request.session["valid_code"] = valid_code

    return HttpResponse(data)


def sign_up(request):
    form = RegisterForm()
    context = {
        'form': form,
    }

    if request.method == "POST":
        register_form = RegisterForm(request.POST)
        res = {'username': None, 'error_dict': None}
        if register_form.is_valid():
            # print(register_form.cleaned_data)

            username = request.POST.get('username')
            password = request.POST.get('password')
            email = request.POST.get('email')
            user = UserInfo.objects.create_user(username=username, password=password, email=email)
            res['username'] = user.username
        else:
            # print(register_form.errors)
            res['error_dict'] = register_form.errors
        return JsonResponse(res)
    return render(request, 'signup.html', context=context)


@login_required(login_url='/user/sign_in')
def logout(request):
    auth.logout(request)
    return redirect('/index')

@login_required(login_url='/user/sign_in')
def profile(request):
    if request.user.is_authenticated:
        if request.method == "POST":
            res = {'msg': None}
            last_name = request.POST.get('last_name')
            school = request.POST.get('school')
            student_id = request.POST.get('student_id')
            mood = request.POST.get('mood')
            telephone = request.POST.get('telephone')
            avatar = request.FILES.get('avatar')
            username = request.user.username

            if avatar:
                avatar_name = 'avatars/' + username + '.' + avatar.name.split('.')[-1]
                result = UserInfo.objects.filter(username=username).update(last_name=last_name, school=school,
                                                                           telephone=telephone,
                                                                           student_id=student_id, mood=mood,
                                                                           avatar=avatar_name)
                with open('media/' + avatar_name, 'wb') as f:
                    for line in avatar:
                        f.write(line)
            else:
                result = UserInfo.objects.filter(username=request.user.username).update(last_name=last_name,
                                                                                        school=school,
                                                                                        telephone=telephone,
                                                                                        student_id=student_id,
                                                                                        mood=mood)
            return JsonResponse(res)
        return render(request, 'profile_info.html')
    # return redirect('/index')
    return render(request, '404.html')

@login_required(login_url='/user/sign_in')
def account_settings(request):
    if request.user.is_authenticated:
        return render(request, 'profile_account.html')
    return render(request, '404.html')

@login_required(login_url='/user/sign_in')
def change_password(request):
    if request.method == "POST":
        res = {'status': False, 'msg': None}
        old_password = request.POST.get('old_password')
        new_password = request.POST.get('new_password')
        confirm_new_password = request.POST.get('confirm_new_password')
        user = UserInfo.objects.filter(username=request.user.username).first()
        # user = request.user
        print()
        if new_password == confirm_new_password:
            if user.check_password(old_password):
                user.set_password(new_password)
                user.save()
                res['status'] = True
            else:
                res['msg'] = "密码错误"
        else:
            res['msg'] = "两次密码不一致"

        return JsonResponse(res)

@login_required(login_url='/user/sign_in')
def change_email(request):
    if request.method == "POST":
        res = {'status': False, 'msg': None}
        current_password = request.POST.get('current_password')
        new_email = request.POST.get('new_email')
        # user = UserInfo.objects.filter(username=request.user.username).first()
        user = request.user
        if user.check_password(current_password):
            UserInfo.objects.filter(username=user.username).update(email=new_email)
            res['status'] = True
        else:
            res['msg'] = "密码错误"

        return JsonResponse(res)



# 以下为管理员操作

@superuser_only
def admin_settings(request):
    user = request.user
    # user.is_superuser
    return render(request, 'admin_manage.html')

@superuser_only
def get_problem_by_id(request):
    if request.is_ajax():
        id = request.POST.get('problem_id')
        res = {'status': False, 'msg': None, 'data':None}
        try:
            problem = Problem.objects.get(id=id)
        except Exception:
            res['msg'] = '无法获取此Id对应的问题'
            return JsonResponse(res)
        res['status'] = True
        res['data'] = {'id':problem.id,'title':problem.title,'level':problem.level,'rule':problem.rule}
        return JsonResponse(res)

@superuser_only
def create_contest(request):
    if request.is_ajax():
        res = {'status': False, 'msg': None}
        c_contest_name = request.POST.get('c_contest_name')
        c_protected = request.POST.get('c_protected',0)
        c_password = request.POST.get('c_password','')
        c_start_time = request.POST.get('c_start_time').replace("T", " ")
        c_end_time = request.POST.get('c_end_time').replace("T", " ")
        start_time = timezone.datetime.strptime(c_start_time, '%Y-%m-%d %H:%M')
        start_time = start_time - datetime.timedelta(hours=8)
        start_time = start_time.replace(tzinfo=utc)
        end_time = timezone.datetime.strptime(c_end_time, '%Y-%m-%d %H:%M')
        end_time = end_time - datetime.timedelta(hours=8)
        end_time = end_time.replace(tzinfo=utc)
        c_description = request.POST.get('c_description','')
        c_orders = request.POST.getlist('c_orders[]')
        c_ids = request.POST.getlist('c_ids[]')

        try:
            with transaction.atomic():
                contest = Contest.objects.create(title=c_contest_name,type=int(c_protected), password=c_password, start_time=start_time,
                                                end_time=end_time, description=c_description,creator_id=request.user.id)
                for i in range(len(c_ids)):
                    problem = Problem.objects.get(id=int(c_ids[i].strip()))
                    Problem_To_Contest.objects.create(problem=problem,contest=contest,p_id=c_orders[i])
        except Exception as e:
            print(repr(e))
            res['msg'] = "创建比赛失败"
            return JsonResponse(res)
        res['status'] = True
        res['msg'] = "创建比赛成功"
        return JsonResponse(res)
    context = {
        'flag':3,  # 3 创建比赛active
    }
    return render(request,'create_contest.html',context=context)


@superuser_only
def create_announcement(request):
    if request.is_ajax():
        res = {'status':False ,'msg':None,'url':None}
        title = request.POST.get('title')
        description = request.POST.get('description')
        try:
            Announcement.objects.create(title=title,description=description,create_author=request.user)
        except Exception as e:
            res['msg'] = '创建公告失败'
            return JsonResponse(res)
        res['status'] = True
        res['msg'] = '创建成功'
        res['url'] = '/index'
        return JsonResponse(res)
    context = {
        'flag': 1, # 创建公告
    }
    return render(request,'create_announcement.html',context=context)

@superuser_only
def add_problem(request):
    if request.is_ajax():
        res = {'status': False, 'msg':None}
        title = request.POST.get('title')
        p_description = request.POST.get('p_description')
        p_input = request.POST.get('p_input')
        p_output = request.POST.get('p_output')
        p_hint = request.POST.get('p_hint')
        simples = request.POST.getlist('simples')
        hide = request.POST.get('hide')
        type = request.POST.get('type').strip()
        if type == 'Standard':
            type = 0
        elif type == "Special":
            type = 1
        else:
            type = 0
        limit_time = request.POST.get('limit_time')
        limit_mem = request.POST.get("limit_mem",0)
        level = request.POST.get('level','Low').strip()
        test_case = request.FILES.get('test_case',0)
        with transaction.atomic():
            problem = Problem.objects.create(title=title, content=p_description, input=p_input, output=p_output, hint=p_hint,
                                            hide=hide,rule=int(type),level=level,time_limit=int(limit_time),mem_limit=int(limit_mem))

            for i in range(0, len(simples), 2):
                s_in = simples[i]
                s_out = simples[i + 1]
                simple_put = Simple_Put.objects.create(simple_input=s_in, simple_output=s_out,problem=problem)
                # problem.simple.add(simple_put)
        t = os.popen('cd ~ ; pwd')
        home_path = t.read()
        home_path = home_path.rstrip()
        project_path = '/acmjudger-master/testcase/'
        tmp = "tmp/"
        filename = home_path + project_path +str(problem.id) + '.zip'
        with open(filename ,'wb') as f:
            for line in test_case:
                f.write(line)
        try:
            # new_filename = home_path + project_path + tmp + str(problem.id) + ".zip"
            # os.system('mv ' + filename + " " + new_filename)
            os.system('unzip ' + filename + ' -d ' + home_path + project_path + str(problem.id))
            res['status'] = True
            res['msg'] = '添加成功'
        except Exception:
            res['msg'] = "文件存储错误"

        return JsonResponse(res)
    print( timezone.localtime(timezone.now()))
    context = {
        'flag': 2,  # 创建公告
    }
    return render(request, 'add_problem.html', context=context)

@superuser_only
@csrf_exempt
def upload(request):
    """用户上传富文本框中点击上传图片到服务器的功能"""
    images = request.FILES.get('upload')
    filename = datetime.datetime.now().strftime("%Y%m%d%H%M%S%f") + images.name
    with open('media/Images/'+filename, 'wb') as f:
        for line in images:
            f.write(line)
    msg = {
        "fileName": "th.jpg",
        "uploaded": 1,
        "error": {
            "number": 0,
            "message": None},
        "url": "/media/Images/" + filename}

    return JsonResponse(msg)
    # return HttpResponse("""
    #                     <script type='text/javascript'>
    #                     window.parent.CKEDITOR.tools.callFunction({0}, '{1}');
    #                     </script>""".format(ck_func_num, """http://127.0.0.1:8000/media/Images/th.jpg"""))
