from django.contrib.auth.models import AbstractUser
from django.db import models


# Create your models here.


class UserInfo(AbstractUser):
    id = models.AutoField(primary_key=True)
    telephone = models.CharField(max_length=11, null=True, unique=True, blank=True)
    avatar = models.FileField(upload_to='avatars/', default="avatars/default.png")
    create_time = models.DateTimeField(verbose_name='创建时间', auto_now_add=True)
    student_id = models.CharField(max_length=20, null=True, unique=True, blank=True)
    school = models.CharField(max_length=64, null=True, blank=True)
    mood = models.CharField(max_length=255, null=True, blank=True)
    ac_number = models.IntegerField(default=0, verbose_name="正确提交数量")
    total_number = models.IntegerField(default=0, verbose_name="全部提交数量")
    problems = models.ManyToManyField(
        to='Problem',
        through='UserInfo_To_Problem',
        through_fields=('user', 'problem'),
    )

    def __str__(self):
        return self.username


class Contest(models.Model):
    """比赛表"""
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=255, verbose_name='比赛标题')
    description = models.TextField(verbose_name='比赛描述', null=True, blank=True)
    # announcement = models.ForeignKey(to="Announcement", to_field='id', null=True, blank=True,
    #                                  on_delete=models.DO_NOTHING, verbose_name='公告')
    status = models.IntegerField(verbose_name='比赛状态', default=0)  # 0 未开始 1 进行中 2 结束
    start_time = models.DateTimeField(verbose_name='开始时间')
    end_time = models.DateTimeField(verbose_name='结束时间')
    type = models.IntegerField(verbose_name='比赛类型', default=0)  # 0 为公开 1 为私有
    password = models.CharField(max_length=32, verbose_name='参加比赛密码', null=True, blank=True)
    creator_id = models.IntegerField(verbose_name='创建者ID')
    users = models.ManyToManyField(
        to='UserInfo',
        through='Contest_To_UserInfo',
        through_fields=('contest', 'user'),
    )

    def __str__(self):
        return str(self.id) + self.title


class Contest_To_UserInfo(models.Model):
    """比赛与用户的多对多表"""
    id = models.AutoField(primary_key=True)
    contest = models.ForeignKey(to='Contest', to_field='id', on_delete=models.CASCADE)
    user = models.ForeignKey(to='UserInfo', to_field='id', on_delete=models.CASCADE)
    ac_number = models.IntegerField(verbose_name='正确提交数量', default=0)
    total_number = models.IntegerField(verbose_name='正确提交数量',default=0)
    total_time = models.CharField(verbose_name='答题总时间',max_length=64,null=True,blank=True,default='0')

    class Meta:
        unique_together = [
            ('contest', 'user')
        ]

    def __str__(self):
        return str(self.id) + "+" + str(self.contest.title) + "+" + str(self.user.id)


class Contest_User_Problem(models.Model):
    """针对单个比赛单个用户对于比赛中所有题解答的记录"""
    id = models.AutoField(primary_key=True)
    problem = models.ForeignKey(to='Problem', to_field='id', on_delete=models.DO_NOTHING)
    contest_To_User = models.ForeignKey(to='Contest_To_UserInfo', to_field='id', on_delete=models.DO_NOTHING)
    ac_number = models.IntegerField(verbose_name='正确提交数',default=0)
    ac_time = models.CharField(verbose_name='真确提交时间', max_length=64,null=True,blank=True)
    wrong_number = models.IntegerField(verbose_name='错误提交数', default=0)

    class Meta:
        unique_together = [
            ('problem', 'contest_To_User')
        ]

    def __str__(self):
        return str(self.id)


class Problem(models.Model):
    """ 问题表，存储问题相关字段"""
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=255, null=False, unique=False, verbose_name='题目标题')
    time_limit = models.IntegerField(default=0, verbose_name='时间限制')
    mem_limit = models.IntegerField(default=0, verbose_name='内存限制')
    ac_number = models.IntegerField(default=0, verbose_name='ac数')
    submit_number = models.IntegerField(default=0, verbose_name='提交数量')
    rule = models.CharField(max_length=255, verbose_name='判题规则', default='0') # 0 为严格比较 1 为 特殊比较
    spj_code = models.TextField(verbose_name='特判代码', null=True, blank=True)
    content = models.TextField(verbose_name='题目描述')
    input = models.TextField(verbose_name='输入描述')
    output = models.TextField(verbose_name='输出描述')
    # simple = models.ForeignKey(to='Simple_Put', to_field='id', on_delete=models.DO_NOTHING)
    hint = models.TextField(verbose_name='提示', null=True,blank=True)
    level = models.CharField(max_length=32, verbose_name='题目难度',default='Low')  #
    hide = models.IntegerField(default=0, verbose_name='时候隐藏')  # 0 不隐藏，1 隐藏
    # 为比赛添加
    contests = models.ManyToManyField(
        to='Contest',
        through='Problem_To_Contest',
        through_fields=('problem', 'contest')
    )
    # simple = models.ManyToManyField(to='Simple_Put')

    def __str__(self):
        return str(self.id) + self.title


class Simple_Put(models.Model):
    id = models.AutoField(primary_key=True)
    problem = models.ForeignKey(to='problem',to_field='id',on_delete=models.CASCADE)
    simple_input = models.TextField(verbose_name='输出样例',null=True,blank=True)
    simple_output = models.TextField(verbose_name='输出样例',null=True,blank=True)


class Problem_To_Contest(models.Model):
    """ 问题与比赛的多对多表"""
    id = models.AutoField(primary_key=True)
    problem = models.ForeignKey(to='Problem', to_field='id', on_delete=models.DO_NOTHING)
    contest = models.ForeignKey(to='Contest', to_field='id', on_delete=models.DO_NOTHING)
    ac_num = models.IntegerField(verbose_name='比赛正确提交次数', default=0)
    total_num = models.IntegerField(verbose_name='比赛全部提交次数', default=0)
    p_id = models.CharField(max_length=255, verbose_name='比赛中的标题',null=True,blank=True)

    class Meta:
        unique_together = [
            ('contest', 'problem')
        ]


class UserInfo_To_Problem(models.Model):  # 用户与问题多对多关系表
    """用户与问题多对多关系表，用来在非比赛的题目中此用户是否AC"""
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(to='UserInfo', to_field='id', on_delete=models.CASCADE)
    problem = models.ForeignKey(to='Problem', to_field='id', on_delete=models.CASCADE)

    class Meta:
        unique_together = [
            ('user', 'problem')
        ]

    def __str__(self):
        return self.user.username + self.problem.title


class Submit(models.Model):
    """题目提交表"""
    solution_id = models.AutoField(primary_key=True, verbose_name='提交编号')
    problem = models.ForeignKey(to='Problem', to_field='id', on_delete=models.DO_NOTHING)
    user = models.ForeignKey(to='UserInfo', to_field='id', on_delete=models.DO_NOTHING)
    language = models.CharField(max_length=64, verbose_name='编程语言')
    # contest_id = models.CharField(verbose_name='比赛编号', max_length=64, null=True)
    contest = models.ForeignKey(verbose_name='比赛编号',to='Contest',to_field='id', null=True,blank=True,on_delete=models.DO_NOTHING)
    submit_time = models.DateTimeField(verbose_name='提交时间',auto_now_add=True)

    def __str__(self):
        return str(self.solution_id)


class Code(models.Model):
    """代码表已submit表一对一用来存储代码与判题相关的参数"""
    id = models.AutoField(primary_key=True)
    submit = models.OneToOneField(to='Submit', to_field='solution_id', on_delete=models.CASCADE)
    time = models.DateTimeField(auto_now_add=True, verbose_name='提交时间')
    content = models.TextField(verbose_name='代码')
    spend_time = models.IntegerField(verbose_name='消耗时间', null=True, blank=True)
    spend_mem = models.IntegerField(verbose_name='消耗内存', null=True, blank=True)
    result = models.IntegerField(verbose_name='结果', default=0)
    message = models.TextField(verbose_name='错误信息', null=True, blank=True)

    def __str__(self):
        return self.result


class Announcement(models.Model):
    """公告表"""
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=255, verbose_name='公告标题')
    description = models.TextField(verbose_name='公告描述')
    belong_contest = models.IntegerField(default=0, verbose_name='是否属于比赛')  # 0 全局，1 属于比赛
    create_time = models.DateTimeField(auto_now_add=True, verbose_name='发布时间')
    create_author = models.ForeignKey(to='UserInfo', to_field='id', verbose_name='发布者', on_delete=models.DO_NOTHING)
    contest = models.ForeignKey(to='Contest',to_field='id',null=True,blank=True,on_delete=models.CASCADE)
    def __str__(self):
        return self.title
