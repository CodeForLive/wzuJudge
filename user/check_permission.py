#!/anaconda3/bin/python
# @Time    : 2019-06-20 16:20
# @Author  : zhou
# @File    : check_permission
# @Software: PyCharm
# @Description: 
#
#
#
# def login_required(func):
#     """这是一个用于计算函数运行时间的装饰器"""
#
#     def deco():
#         if request
#         func()
#
#
#
#     return deco

from django.core.exceptions import PermissionDenied


def superuser_only(function):
    """Limit view to superusers only."""

    def _inner(request, *args, **kwargs):
        if not request.user.is_superuser:
            raise PermissionDenied
        return function(request, *args, **kwargs)

    return _inner
