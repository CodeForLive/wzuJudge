from django.shortcuts import render

# Create your views here.
from user.models import Announcement


def index(request):
    announcements = Announcement.objects.filter(belong_contest=0).order_by('-create_time')
    context = {
        'announcements' : announcements
    }
    return render(request, 'index.html', context=context)

def about(request):
    return render(request, 'about.html')

def temp(request):
    return render(request,'testtemplate.html')


def page_not_found(request):
    return render(request,'404.html')

def error(request):
    return render(request,'500.html')